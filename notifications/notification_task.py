from django.db.models.signals import post_save
from django.dispatch import receiver
from users.models import UserProxy
from .models import NewReportNotification, ReviewNotification
from . import push_service


@receiver(post_save, sender=NewReportNotification)
def send_notification(sender, instance, **kwargs):
    title = instance.header
    content = instance.get_message()
    registration_ids = [UserProxy.objects.get(id=instance.receiver_id).device_id, ]

    data_payload = {
        "notification_id": instance.id
    }

    result = push_service.notify_multiple_devices(registration_ids=registration_ids, message_title=title,
                                                  message_body=content, data_message=data_payload)

    print(result)


def send_notification2(notification):
    title = notification.header
    content = notification.message
    registration_ids = [UserProxy.objects.get(id=notification.receiver_id).device_id, ]

    data_payload = {
        "notification_id": notification.id
    }

    result = push_service.notify_multiple_devices(registration_ids=registration_ids,
                                                  message_title=title,
                                                  message_body=content,
                                                  data_message=data_payload)


