from django.db.models.signals import post_save
from django.dispatch import receiver

from users.models import User, UserProxy
from djongo import models
from . import push_service
from users.models import Report


class Notification(models.Model):
    creation_date = models.DateTimeField(auto_now_add=True)
    header = models.CharField(max_length=100)
    sender_id = models.IntegerField()
    receiver_id = 0
    status = models.CharField(max_length=10, choices=[('VIEWED', 'VIEWED'), ('FORWARDED', 'FORWARDED')])

    class Meta:
        abstract = True


class ReviewNotification(Notification):
    header = models.CharField(max_length=20, default="Report Review")
    report_id = models.IntegerField()
    message = models.CharField(max_length=150, default="Your report has been reviewed!")

    def get_message(self):
        return self.message + " The report is {}".format(Report.objects.get(id=self.report_id).title)


class NewReportNotification(Notification):
    header = models.CharField(max_length=20, default="New Report")
    report_id = models.IntegerField()
    message = models.CharField(max_length=150, default="A new report sended by ")

    def get_message(self):
        return self.message + "{}".format(User.objects.get(id=self.sender_id))


