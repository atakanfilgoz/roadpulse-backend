from django.apps import AppConfig
from django.conf import settings


class NotificationsConfig(AppConfig):
    name = 'notifications'

    def ready(self):
        import notifications.notification_task
