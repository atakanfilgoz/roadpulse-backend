from pyfcm import fcm
from django.conf import settings

__all__ = "push_service"

push_service = fcm.FCMNotification(api_key=settings.FIREBASE_FCM_KEY)
