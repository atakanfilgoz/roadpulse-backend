This is the backend server of roadpulse application.
To run server, follow the steps:

1)Create a virtual environment in the project root directory:
	python3 -m venv env

2)Activate newly created environment:
	source <project_root>/env/bin/activate

3)Install dependencies
	pip3 install -r requirements.txt
	
4)Run server
	python3 manage.py roadpulse/manage.py runserver