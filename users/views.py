from django.core.exceptions import FieldDoesNotExist
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.utils import timezone
from rest.utility.auth_decorators import *
import json
from .models import *
from notifications.notification_task import send_notification2
# Create your views here.
from djongo import models
from notifications.models import *

from rest import converter_engine
from rest.utility.JsonToModel import json_to_and_query, mongo_to_dict

import traceback


@csrf_exempt
@custom_login_required
@role_required(roles=["Admin", "Repairman"])
def delete_report(request):
    data = request.body
    if "Admin" in request.user.roles:
        if data['report_id'] == "all":
            Report.objects.all().delete()
        else:
            for report_id in data['report_id']:
                Report.objects.get(id=report_id).delete()
    else:
        if data['report_id'] == "all":
            Report.objects.filter(sender_id=request.user.id).delete()
        else:
            for report_id in data['report_id']:
                if Report.objects.get(id=report_id).sender_id == request.user.id:
                    Report.objects.get(id=report_id).delete()
                else:
                    return JsonResponse("The query sended to server is not allowed with these credentials.", status=401)

    return JsonResponse("Delete successful.", safe=False)


@csrf_exempt
@custom_login_required
@role_required(roles=["Admin", "Repairman"])
def get_reports(request):
    try:
        data = json.loads(request.body)
    except:
        return JsonResponse("Bad request!", status=400)

    raw_query = data.get('query', None)
    mongo_query = {'$and': []}
    if "Admin" in request.user.roles:
        if raw_query is None:
            mongo_query['$and'] = [{"sender_id": {"$ne": None}}]
    else:
        if raw_query is not None:
            if "sender_id" in raw_query:
                return JsonResponse("Forbidden query", safe=False, status=403)
        mongo_query['$and'] = [{"sender_id": request.user.id}]

    if raw_query is not None:
        mongo_query['$and'].extend(json_to_and_query(raw_query))

    print(mongo_query)

    reports = [mongo_to_dict(report) for report in Report.objects.mongo_find(mongo_query, {"_id": 0})]
    return JsonResponse(reports, safe=False)


@csrf_exempt
@custom_login_required
@role_required(roles=["Admin"])
def reply_report(request):
    data = json.loads(request.body)
    report = Report.objects.filter(id=data['replied_report_id'])[0]
    report.reply_text = data['text']
    report.reply_date = timezone.now()
    report.status = data['reply_status']
    report.reviewer_id = request.user.id
    validated_pothole_ids = data['validated_pothole_ids']
    report.validated_potholes = validated_pothole_ids
    for fp_id in validated_pothole_ids:
        p = Pothole.objects.get(id=fp_id)
        p.status = "FIXED"
        p.save()

    report.denied_potholes = data['denied_pothole_ids']

    report.save()

    review_notf=ReviewNotification()
    review_notf.status="FORWARDED"
    review_notf.sender_id=request.user.id
    review_notf.receiver_id=report.sender_id
    review_notf.report_id=report.id
    review_notf.save()
    send_notification2(review_notf)

    return JsonResponse("Report Replied.", safe=False)


@csrf_exempt
def send_report(request):
    data = json.loads(request.POST.get('report'))
    new_report = Report()
    new_report.text = data['text']
    new_report.title = data['title']
    new_report.sender_id = request.user.id

    image_mapping = json.loads(request.POST.get("image_mapping"))

    fixed_pothole_list = []
    for fixed_pothole in image_mapping:
        fp = FixedPothole()
        fp.pothole_id = fixed_pothole['id']

        img_list = []

        for img_name in fixed_pothole['images']:
            img = request.FILES[img_name].file
            upload_result = converter_engine.upload(img)
            new_img = Image()
            new_img.link = upload_result['url']
            new_img.public_id = upload_result['public_id']
            img_list.append(new_img)

        fp.images = img_list
        fixed_pothole_list.append(fp)

    new_report.fixed_potholes = fixed_pothole_list
    new_report.status = Report.NOTSEEN
    new_report.validated_potholes = []
    new_report.denied_potholes = []
    new_report.save()

    return JsonResponse("Report successfully added.", safe=False)


@csrf_exempt
def get_user(request):
    query = json.loads(request.body)
    users = [p.to_dict() for p in User.objects.filter(**query)]
    return JsonResponse(users, safe=False)
