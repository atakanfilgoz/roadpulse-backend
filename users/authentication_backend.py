from django.contrib.auth import get_user_model

user_model = get_user_model()


class EmailBackend:
    def authenticate(self, request, email, password, **kwargs):
        try:
            user = user_model.objects.get(email=email)
        except user_model.DoesNotExist:
            return None
        if user.check_password(password):
            return user
        return None

    def get_user(self, user_id):
        try:
            user = user_model.objects.get(pk=user_id)
        except user_model.DoesNotExist:
            return None
        return user