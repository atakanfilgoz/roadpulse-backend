from django.contrib.auth.models import AbstractBaseUser
from djongo import models
from rest.utility.ExtendedModels import *
from potholes.models import *

# Create your models here.
from django.utils.crypto import get_random_string
from notifications.models import *
from task_management.models import *


class User(AbstractBaseUser, MetaModel):
    last_login = None
    name = models.CharField(max_length=100, blank=True)
    surname = models.CharField(max_length=100)
    email = models.EmailField(unique=True)
    is_admin = models.BooleanField(default=False)
    roles = models.ListField(default=["Repairman"])
    device_id = models.TextField(null=True)

    USERNAME_FIELD = 'email'
    EMAIL_FIELD = 'email'
    REQUIRED_FIELDS = []

    def __str__(self):
        return "{}-{}".format(self.name, self.email)

    def __getattr__(self, item):
        pass

    def get_full_name(self):
        return "%s %s".format(self.name, self.surname)

    def has_perm(self, perm, obj=None):
        return self.is_admin

    def has_module_perms(self, app_label):
        return self.is_admin

    @property
    def is_staff(self):
        return self.is_admin


class UserManager(models.DjongoManager):

    @classmethod
    def normalize_email(cls, email):
        email = email or ''
        try:
            email_name, domain_part = email.strip().rsplit('@', 1)
        except ValueError:
            pass
        else:
            email = email_name + '@' + domain_part.lower()
        return email

    def make_random_password(self, length=10,
                             allowed_chars='abcdefghjkmnpqrstuvwxyz'
                                           'ABCDEFGHJKLMNPQRSTUVWXYZ'
                                           '23456789'):

        return get_random_string(length, allowed_chars)

    def get_by_natural_key(self, username):
        return self.get(**{self.model.USERNAME_FIELD: username})

    def create_user(self, email, password, **kwargs):
        user = self.model(email=email, **kwargs)
        user.set_password(password)
        user.save()
        return user

    def create_repairman(self, email, password, cid, **kwargs):
        user = Repairman(email=email, password=password, client_id=cid, **kwargs)
        user.set_password(password)
        user.save()
        return user

    def create_detector(self, email, password, cid, **kwargs):
        user = Detector(email=email, password=password, client_id=cid, roles=["Detector"], **kwargs)
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, email, password, **kwargs):
        new_admin = self.create_user(email, password, roles=["Admin"], **kwargs)
        new_admin.is_admin = True
        new_admin.save()
        return new_admin


class Repairman(User):
    obj_id = models.ObjectIdField()
    client_id = models.CharField(max_length=250)
    objects = UserManager()
    task = models.ArrayField(model_container=Repair, null=True)


class Detector(User):
    obj_id = models.ObjectIdField()
    client_id = models.CharField(max_length=250)
    objects = UserManager()
    task = models.ArrayField(model_container=Detection)


class UserProxy(User):
    class Meta:
        proxy = True

    objects = UserManager()


class Report(MetaModel):
    DENIED = 'DENIED'
    PASSED = 'PASSED'
    NOTSEEN = 'NOTSEEN'

    REPORT_STATUS = [
        (NOTSEEN, "NOTSEEN"),
        (DENIED, "DENIED"),
        (PASSED, "PASSED")

    ]

    objects = models.DjongoManager()

    sender_id = models.IntegerField()
    creation_date = models.DateTimeField(auto_now_add=True)
    text = models.TextField()
    title = models.CharField(max_length=50)
    status = models.CharField(choices=REPORT_STATUS, max_length=20, default=NOTSEEN)
    fixed_potholes = models.ArrayField(model_container=FixedPothole, null=True)
    reply_date = models.DateTimeField(null=True)
    reply_text = models.TextField(null=True)
    reviewer_id = models.IntegerField(null=True)
    validated_potholes = models.ListField(null=True)
    denied_potholes = models.ListField(null=True)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        from notifications.models import NewReportNotification, ReviewNotification
        if self.id is None:
            new_notification = NewReportNotification()
            new_notification.sender_id = self.sender_id
            new_notification.status = "FORWARDED"
            new_notification.report_id = self.id
            new_notification.receiver_id = 0
            new_notification.save()


        super().save(force_insert=force_insert, force_update=force_update, using=using, update_fields=update_fields)