from django.utils.deprecation import MiddlewareMixin
from django.http import HttpRequest
from users import logged_in_users
from users.models import *


class ReflectMiddleWare(MiddlewareMixin):
    def process_request(self, request: HttpRequest):
        if request.META.get('HTTP_AUTH', None):
            if request.COOKIES.get("sessionid", None) is None:
                request.COOKIES['sessionid'] = request.META['HTTP_AUTH']


class UserCache(MiddlewareMixin):
    def process_request(self, request: HttpRequest):
        if request.user.is_authenticated:
            if request.user.id in logged_in_users:
                request.user_t = logged_in_users['id']
            else:
                if "Admin" in request.user.roles:
                    logged_in_users['id'] = User.objects.get(id=request.user.id)
                elif "Repairman" in request.user.roles:
                    logged_in_users['id'] = Repairman.objects.get(id=request.user.id)
                elif "Detector" in request.user.roles:
                    logged_in_users['id'] = Detector.objects.get(id=request.user.id)

                request.user_t = logged_in_users['id']
