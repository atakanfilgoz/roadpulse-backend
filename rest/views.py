from django.http import HttpResponse, JsonResponse, HttpResponseNotFound
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import authenticate, login, logout
from django.core.exceptions import ObjectDoesNotExist
from potholes.models import Pothole, DetectedPotholes, Image as PImage, Address
from .utility import JsonToModel
from .utility.auth_decorators import custom_login_required, role_required
from users.authentication_backend import EmailBackend
from . import *
from .utility.Converter import Request
import json
import traceback
from notifications.models import *
from PIL import Image as PIL_Image, ImageFont, ImageDraw
from django.shortcuts import get_object_or_404
from users.models import *


# import matplotlib.pyplot as plt


# Create your views here.


@csrf_exempt
def test(request):
    '''
    print(request.FILES['file'].file) #returns byteio
    cnv=YuvToJpgConverter()
    img=cnv.convert(request.FILES['file'].file.getvalue(),(240,214))
    img.save("/home/togayyazar/Desktop/SELAMMM.jpeg","JPEG")
    print(json.loads(request.POST.get("name")))
    print(json.loads(request.POST.get("soyad")))


    memory_buf = request.FILES['file'].file
    res = json.loads(request.POST['img_res']).get('resolution')
    img_format = request.POST['format']
    pothole = request.POST.get("pothole", None)
    convert_request = Request(memory_buf.getvalue(), res,img_format)
    img_buf = converter_engine.convert(convert_request)
    result=converter_engine.upload(img_buf)

    pothole_instances = JsonToModel.create_model(pothole, Pothole)
    for pothole in pothole_instances:
        try:
            pothole.save()
        except Exception as e:
            return JsonResponse({"error_msg": "An error has occured during saving potholes."})
    '''
    print(request.user_t.to_dict())

    return HttpResponse("oldu")


@csrf_exempt
def get_detected_potholes(request):
    detection_record = get_object_or_404(DetectedPotholes, user_id=request.user.id)
    detected_pothole_list = detection_record.potholes
    front = detected_pothole_list.pop(0)
    if len(detected_pothole_list) == 0:
        detection_record.delete()
    else:
        detection_record.potholes = detected_pothole_list
        detection_record.save()

    return JsonResponse(front, safe=False)


@csrf_exempt
def add_pothole_geocode(request):
    if request.POST.get('pothole', None):
        data = json.loads(request.POST.get('pothole'))
    else:
        data = json.loads(request.body)
    geo_result = Pothole.objects.reverse_geocode(data['lat'], data['long'])
    pothole_address = Address(**geo_result)
    p = Pothole()
    p.addr = pothole_address
    p.latitude = data['lat']
    p.longitude = data['long']
    p.label = data['label']
    pixels = data["pixels"]
    print(pixels)
    image = PImage()
    print(request.FILES.keys())
    if not request.FILES.get('img_file', None) is None:
        img = request.FILES['img_file'].file
        source_img = PIL_Image.open(img)
        draw = ImageDraw.Draw(source_img)
        for square in pixels:
            w = int(square["w"] * 720)
            x = int(square["x"] * 720)
            h = int(square["h"] * 480)
            y = int(square["y"] * 480)
            start_point = (int(x - w / 2), int(y - h / 2))
            end_point = (int(x + w / 2), int(y + h / 2))
            draw.rectangle((start_point, end_point))
            buffer = io.BytesIO()
            source_img.save(buffer, "PNG")
        result = converter_engine.upload(buffer)
        image.public_id = result['public_id']
        image.link = result['url']
    p.image = image
    p.status = "NOT-FIXED"
    p.save()

    obj_list = DetectedPotholes.objects.filter(user_id=request.user.id)

    if len(obj_list) == 0:
        d_pothole = DetectedPotholes()
        d_pothole.potholes = [p.to_dict()]
        d_pothole.user_id = request.user.id
        d_pothole.save()
    else:
        old = obj_list[0].potholes
        old.append(p.to_dict())
        obj_list[0].potholes = old
        obj_list[0].save()

    return JsonResponse({"ok_msg": "Pothole successfully added."}, safe=False)


@csrf_exempt
def add_potholes(request):
    pothole = request.body
    result = None
    if not request.FILES.get('raw_file', None) is None:
        memory_buf = request.FILES['raw_file'].file
        res = json.loads(request.POST['img_res']).get('resolution')
        img_format = request.POST['format']
        pothole = request.POST.get("pothole", None)
        convert_request = Request(memory_buf.getvalue(), res, img_format)
        img_buf = converter_engine.convert(convert_request)
        result = converter_engine.upload(img_buf)

    elif not request.FILES.get('img_file', None) is None:
        '''supports jpeg and png'''
        img = request.FILES['img_file'].file
        pothole = request.POST.get("pothole")
        result = converter_engine.upload(img)

    pothole_instances = JsonToModel.create_model(pothole, Pothole)

    for pothole in pothole_instances:
        try:
            image = PImage()
            if result is not None:
                image.public_id = result.get('public_id')
                image.link = result.get('url')

            pothole.image = image

            pothole.save()
        except Exception as e:
            print(traceback.format_exc())
            return JsonResponse({"error_msg": "An error has occured during saving potholes."}, status=500)

    return JsonResponse({"ok_msg": "Pothole successfully added."}, safe=False)


@custom_login_required
def get_potholes(request):
    potholes = [p.to_dict() for p in Pothole.objects.all()]
    for p in potholes:
        p['label'] = p.get('label').upper()
    return JsonResponse(potholes, safe=False)


@csrf_exempt
def new_get_potholes(request):
    if request.method == 'GET':
        potholes = [p.to_dict() for p in Pothole.objects.all()]
        for p in potholes:
            p['label'] = p.get('label').upper()
        return JsonResponse(potholes, safe=False)
    else:
        query = json.loads(request.body)
        potholes = [p.to_dict() for p in Pothole.objects.filter(**query)]
        return JsonResponse(potholes, safe=False)


@csrf_exempt
def delete_potholes(request, id):
    try:
        p = Pothole.objects.get(id=id)

    except ObjectDoesNotExist as e:
        return HttpResponseNotFound()

    p.delete()

    return HttpResponse("Pothole Deleted successfully")


@csrf_exempt
def api_auth(request):
    if request.method == 'POST':
        data = json.loads(request.body)
        u_email = data['email']
        u_password = data['password']
        user = authenticate(request, email=u_email, password=u_password)
        if user is not None:
            login(request, user)
            if data.get('device_id', None) is not None:
                user.device_id = data['device_id']
                user.save(update_fields=['device_id'])
            if "Detector" in user.roles:
                request.session['detected_potholes'] = []
            response = JsonResponse({"msg": "Login  is successful.", "is_admin":request.user.is_admin}, status=200)
            return response

        return JsonResponse({"msg": "Authentication Failed. Try again.", }, status=403)
    if request.method == 'GET':
        if request.user.is_authenticated:
            return JsonResponse("You're already logged in.", safe=False)
        return JsonResponse("Send credentials with JSON inside a HTTP POST request.", safe=False)


def api_logout(request):
    logout(request)
    return JsonResponse("Logout successful.", safe=False)


@csrf_exempt
def delete_all(request):
    Pothole.objects.delete_all()
    return JsonResponse({"msg": "All potholes succesfully delete"})


@csrf_exempt
def update(request):
    query_obj_as_dict = json.loads(request.body)
    p_id = query_obj_as_dict['id']
    p_obj = Pothole.objects.get(id=p_id)
    Pothole.objects.u_from_dict(query_obj_as_dict, p_obj)
    p_obj.save()
    return JsonResponse("Model successfully updated.", safe=False)


@csrf_exempt
def filter_potholes(request):
    d = json.loads(request.body)
    q = JsonToModel.json_to_mongo_query(d)
    potholes = [Pothole.objects.get(id=p['id']).to_dict() for p in Pothole.objects.mongo_find({'$and': q})]
    return JsonResponse(potholes, safe=False)
