from django.urls import path, re_path
from . import views
from routes import views as route_views
from users import views as user_views
from task_management import views as task


app_name = 'rest'


urlpatterns = [
    path('addPotholes/', views.add_potholes),
    re_path(r'getPotholes/', views.new_get_potholes),
    path('deletePothole/<int:id>', views.delete_potholes),
    path('api_auth/', views.api_auth),
    path('test/', views.test),
    path('api_logout/', views.api_logout),
    path('deletePotholes/', views.delete_all),
    path('updatePothole/', views.update),
    path('filterPothole/', views.filter_potholes),
    path('getRoutes/', route_views.get_routes),
    path('addRoute/', route_views.add_route),
    path('assignTask/',task.assign_task),
    path('getTask/',task.get_task),
    path('addPotholeCoord/', views.add_pothole_geocode),
    path('sendReport/', user_views.send_report),
    re_path(r'getReports/', user_views.get_reports),
    path('replyReport/', user_views.reply_report),
    path('getDetectedPotholes/',views.get_detected_potholes),
    path('deleteTask/<int:u_id>',task.delete_task),
    path('getUser/',user_views.get_user)
]

