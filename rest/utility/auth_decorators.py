from django.http import JsonResponse


def custom_login_required(function):
    def wrapper(request, *args, **kw):
        if not request.user.is_authenticated:
            return JsonResponse('Permission denied.', safe=False, status=401)
        else:
            return function(request, *args, **kw)

    return wrapper


def role_required(roles):
    def decorator(function):
        def wrapper(request, *args, **kwargs):
            for role in roles:
                if role in request.user.roles:
                    return function(request, *args, **kwargs)

            return JsonResponse('Permission denied.', safe=False, status=403)
        return wrapper
    return decorator
