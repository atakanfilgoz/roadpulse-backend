import json
from collections import OrderedDict

from djongo.models.fields import EmbeddedField, ArrayField


def create_model_util(dict_obj, model):
    for k, v in dict_obj.items():
        ins_field = model._meta.get_field(k)
        if isinstance(ins_field, EmbeddedField) or isinstance(ins_field, ArrayField):
            nested_obj = create_model_util(dict_obj.get(k), ins_field.model_container)
            dict_obj[k] = nested_obj

    return model(**dict_obj)


def create_model(json_data, model):
    json_obj_list = json.loads(json_data)

    if not isinstance(json_obj_list, list):
        json_obj_list = [json_obj_list]

    model_list = [create_model_util(dict_obj, model) for dict_obj in json_obj_list]

    return model_list


def json_to_mongo_query(data):
    q_list = []
    for key, value in data.items():
        if not isinstance(value, dict):
            q_list.append({key: {'$and': value}})
        else:
            sub = json_to_mongo_query(value)
            for dict_ins in sub:
                k = list(dict_ins.keys())[0]
                v = dict_ins[k]
                new_key = "{}.{}".format(key, k)
                q_list.append({new_key: v})
    return q_list


def json_to_and_query(data):
    q_list = []
    for key, value in data.items():
        if not isinstance(value, dict):
            if isinstance(value,str) and "-" in value:
                or_list = [{key: or_value} for or_value in value.split("-")]
                q_list.append({"$or": or_list})
            else:
                q_list.append({key: value})
        else:
            sub = json_to_and_query(value)
            print(sub)
            for dict_ins in sub:
                k = list(dict_ins.keys())[0]
                if k == "$or":
                    pairs = []
                    for sub_dict in dict_ins[k]:
                        k1 = "{}.{}".format(key, list(sub_dict.keys())[0])
                        v1 = sub_dict[list(sub_dict.keys())[0]]
                        pairs.append({k1: v1})
                    q_list.append({'$or': pairs})
                else:
                    v = dict_ins[k]
                    new_key = "{}.{}".format(key, k)
                    q_list.append({new_key: v})
    return q_list


def mongo_to_dict(param):
    d = {}
    for k, v in param.items():
        if isinstance(v, OrderedDict):
            d[k] = mongo_to_dict(v)
        elif isinstance(v, list):
            l = []
            for element in v:
                if isinstance(v, OrderedDict):
                    d2 = mongo_to_dict(v)
                    l.append(d2)
                elif isinstance(v, list):
                    dummy = {"key": element}
                    result = mongo_to_dict(dummy)
                    l.append(result['key'])
                else:
                    l.append(element)

            d[k] = l
        else:
            d[k] = v
    return d
