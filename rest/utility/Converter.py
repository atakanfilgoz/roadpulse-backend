from abc import ABC, abstractmethod
from typing import Optional, Union, Tuple, Dict, List
import io
import cloudinary
import cloudinary.uploader
import cv2
from PIL import Image
import numpy
from django.conf import settings


class Singleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


# Abstract Factory
class ConverterFactory(ABC):
    """
    Generate converter(Product) and use its functionality
    """

    @abstractmethod
    def create_yuv_to_jpg_converter(self):
        pass

    def create_bgra_to_jpg_converter(self):
        pass

    '''
    def convert_img_to_jpg(self, source, dest, resolution):
        converter = create_converter(resolution)
        img = converter.convert(source, dest)
        img.save(dest, "JPEG")
        img.close()

    '''


# Concrete Factory class
class ConcreteFactory(ConverterFactory):

    def create_yuv_to_jpg_converter(self):
        return YuvToJpgConverter()

    def create_bgra_to_jpg_converter(self):
        return BgraToJpgConverter()


# Abstract Product
class Converter(ABC):
    @abstractmethod
    def convert(self, source, resolution: tuple = None):
        pass


class BgraToJpgConverter(Converter):

    def convert(self, source, resolution: tuple = None):
        nparr = numpy.fromstring(source, numpy.uint8)
        img_np = cv2.imdecode(nparr, cv2.CV_LOAD_IMAGE_COLOR)
        rgb_img = cv2.cvtColor(img_np, cv2.COLOR_BGRA2RGB)
        img = Image.fromarray(rgb_img)
        return img


class YuvToJpgConverter(Converter):

    def convert(self, source, resolution: tuple = None):
        '''
        print(resolution)
        if resolution is None:
            raise Exception("You have to provide valid resolution for the image being converted.")

        with open(source, "rb") as f:
            yuv = f.read()
        '''
        rgb_bytes = YuvToJpgConverter.__yuv420_to_rgb888(*resolution, source)
        img = Image.frombytes("RGB", resolution, bytes(rgb_bytes))
        return img

    @staticmethod
    def __yuv420_to_rgb888(width, height, yuv):
        # function requires both width and height to be multiples of 4
        rgb_bytes = bytearray(width * height * 3)

        red_index = 0
        green_index = 1
        blue_index = 2
        y_index = 0
        for row in range(0, height):
            u_index = width * height + (row // 2) * (width // 2)
            v_index = u_index + (width * height) // 4
            for column in range(0, width):
                Y = yuv[y_index]
                U = yuv[u_index]
                V = yuv[v_index]
                C = (Y - 16) * 298
                D = U - 128
                E = V - 128
                R = (C + 409 * E + 128) // 256
                G = (C - 100 * D - 208 * E + 128) // 256
                B = (C + 516 * D + 128) // 256
                R = 255 if (R > 255) else (0 if (R < 0) else R)
                G = 255 if (G > 255) else (0 if (G < 0) else G)
                B = 255 if (B > 255) else (0 if (B < 0) else B)
                rgb_bytes[red_index] = R
                rgb_bytes[green_index] = G
                rgb_bytes[blue_index] = B
                u_index += (column % 2)
                v_index += (column % 2)
                y_index += 1
                red_index += 3
                green_index += 3
                blue_index += 3
        return rgb_bytes


class Request:
    def __init__(self, img: bytes, resolution: Union[Tuple, Dict], img_format: str):
        self.img = img
        self.resolution = resolution
        self.img_format = img_format

    @property
    def resolution(self):
        return self.__resolution

    @resolution.setter
    def resolution(self, resolution: Union[Tuple, Dict]):
        if resolution[0] <= 0 and resolution[0] <= 0:
            raise Exception("Too small resolution values")

        self.__resolution = resolution

    @property
    def img_format(self):
        return self.__img_format

    @img_format.setter
    def img_format(self, value):
        value = value.lower()
        self.__img_format = value


class RequestHandler(ABC):
    def __init__(self, next_handler):
        self.next = next_handler

    @abstractmethod
    def handle(self, request: Request):
        pass

    @property
    def next(self):
        return self.__next

    @next.setter
    def next(self, next_handler):
        if next_handler is None:
            self.__next = next_handler
            return

        if not isinstance(next_handler, RequestHandler):
            raise Exception("Next handler must be a type of RequestHandler")
        self.__next = next_handler

    def convey(self, request: Request):
        if self.next is None:
            raise Exception("Unsupported Format.")
        self.next.handle(request)


class BgraHandler(RequestHandler):
    def __init__(self, next_handler=None, converter: Optional[Converter] = None):
        super().__init__(next_handler)
        if converter is None:
            self.converter = BgraToJpgConverter()
        else:
            self.converter = converter

    def handle(self, request: Request):
        if not request.img_format == "bgra":
            return self.convey(request)
        else:
            buffer = io.BytesIO()
            img = self.converter.convert(request.img)
            img.save(buffer, "JPEG")
            img.close()
            return buffer

    def __repr__(self):
        return "BgraHandler"

    @property
    def converter(self):
        return self.__converter

    @converter.setter
    def converter(self, value: Converter):
        if not isinstance(value, Converter):
            raise Exception("Bad converter!")
        self.__converter = value


class YuvHandler(RequestHandler):
    def __init__(self, next_handler=None, converter: Optional[Converter] = None):
        super().__init__(next_handler)
        if converter is None:
            self.converter = YuvToJpgConverter()
        else:
            self.converter = converter

    def handle(self, request: Request):
        if not request.img_format == "yuv":
            return self.convey(request)
        else:
            buffer = io.BytesIO()
            img = self.converter.convert(request.img, request.resolution)
            img.save(buffer, "JPEG")
            img.close()
            return buffer

    @property
    def converter(self):
        return self.__converter

    @converter.setter
    def converter(self, value):
        if not isinstance(value, Converter):
            raise Exception("Bad converter!")
        self.__converter = value

    def __repr__(self):
        return "YuvHandler"


class ConverterEngine(metaclass=Singleton):
    default_chain = ["yuv", "bgra"]
    converter_factory = ConcreteFactory()

    def __init__(self):
        self.result = None
        self.chain_head: RequestHandler = None
        self.upload_manager = CloudinaryManager()
        self.chain_tail: RequestHandler = None
        self.upload_manager.configure(
            cloud_name="dkbn6q63t",
            api_key="422643585513332",
            api_secret="HO_p6oWeI5TeZya4EnsRmTj7Opk"
        )

    def configure(self, chain: Optional[List[str]] = None):
        if chain is None:
            chain = ConverterEngine.default_chain

        for index in range(len(chain)):
            handler = ConverterEngine.handler_generator(chain[index])
            self.add_handler_last(handler)

    def traverse_chain(self):
        walker = self.chain_head
        while not walker is None:
            print(walker)
            walker = walker.next

    @staticmethod
    def handler_generator(handler_type: str):
        if handler_type.lower() == "yuv":
            return YuvHandler(None, ConverterEngine.converter_factory.create_yuv_to_jpg_converter())

        if handler_type.lower() == "bgra":
            return BgraHandler(None, ConverterEngine.converter_factory.create_bgra_to_jpg_converter())

        raise Exception("Unsupported Handler")

    def add_handler_last(self, handler: RequestHandler):
        if self.chain_head is None:
            self.chain_head = handler
            self.chain_tail = handler
        else:
            self.chain_tail.next = handler
            self.chain_tail = handler

    def convert(self, request: Request):
        buffer_file = self.chain_head.handle(request)
        return buffer_file

    def upload(self, buffer):
        result = self.upload_manager.upload_photo(buffer)
        buffer.close()
        return result


class CloudinaryManager(ABC):

    def __init__(self):
        super().__init__()

    @staticmethod
    def configure(cloud_name, api_key, api_secret):
        cloudinary.config(
            cloud_name=cloud_name,
            api_key=api_key,
            api_secret=api_secret
        )

    def upload_photo(self, source: io.BytesIO):
        return cloudinary.uploader.upload(source.getvalue())

    def delete_photo(self,public_id):
        return cloudinary.uploader.destroy(public_id)