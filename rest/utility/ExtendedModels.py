import djongo
from djongo import models


class MetaModel(models.Model):
    class Meta:
        abstract = True

    def to_dict(self):
        def list_to_dict(obj_list):
            data1 = []
            for element in obj_list:
                if isinstance(element, djongo.models.Model):
                    data1.append(element.to_dict())
                elif isinstance(element, list):
                    nested_l = list_to_dict(element)
                    data1.append(nested_l)
                else:
                    data1.append(element)

            return data1

        data = {}

        for col in self._meta.concrete_fields:
            if isinstance(col, djongo.models.EmbeddedField):
                embedded_obj = col.value_from_object(self)
                embedded_dict = embedded_obj.to_dict()
                data[col.name] = embedded_dict
            elif isinstance(col, djongo.models.ArrayField):
                obj_l = list_to_dict(col.value_from_object(self))
                data[col.name] = obj_l
            else:
                data[col.name] = col.value_from_object(self)

        return data
