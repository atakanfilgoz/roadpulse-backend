from itertools import chain

from djongo import models
from django.utils import timezone
from rest.utility.ExtendedModels import *



class Point(MetaModel):
    longitude = models.FloatField()
    latitude = models.FloatField()

    class Meta:
        abstract = True

    def __str__(self):
        return "[ latitude: {}, longitude: {} ]".format(self.longitude, self.latitude)

    def __repr__(self):
        return "<Point - {} > ".format(str(self))

    def to_dict(self):
        return {'latitude':self.latitude,'longitude':self.longitude}


class Route(MetaModel):
    user_id = models.IntegerField()
    creation_date = models.DateTimeField(auto_now_add=True)
    points = models.ArrayField(model_container=Point)

    objects = models.DjongoManager()

    def to_dict(self):
        meta_info = self._meta
        data = {}
        for attribute in chain(meta_info.concrete_fields, meta_info.private_fields):
            if attribute.name == 'points':
                coords=[]
                for p in self.points:
                    coords.append(p.to_dict())
                data[attribute.name]=coords
            else:
                data[attribute.name] = attribute.value_from_object(self)

        return data



