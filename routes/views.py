from django.http import JsonResponse

# Create your views here.
from django.views.decorators.csrf import csrf_exempt
from rest.utility.auth_decorators import custom_login_required
from users.models import *
from .models import *
import json
from django.db.models import Q


@csrf_exempt
def add_route(request):
    json_dict = json.loads(request.body)
    new_route = Route()
    new_route.user_id = 1
    point_list = []
    for point in json_dict['coordinates']:
        new_point = Point()
        new_point.latitude = point['latitude']
        new_point.longitude = point['longitude']
        point_list.append(new_point)

    new_route.points = point_list
    new_route.save()

    return JsonResponse("Route is successfully added.", safe=False)


def get_routes(request):
    objs = [r.to_dict() for r in Route.objects.filter(~Q(user_id_=None))]
    return JsonResponse(objs, safe=False)
