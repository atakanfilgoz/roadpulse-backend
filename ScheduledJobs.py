from pymongo import MongoClient
from apscheduler.schedulers.background import BackgroundScheduler
import datetime
from pyfcm import fcm

client = MongoClient('mongodb+srv://admin2:159753asd@cluster0-s8rs2.mongodb.net/test?retryWrites=true&w=majority')
db = client.db
repairman_collection = db.users_repairman
user_collection = db.users_user

sched = BackgroundScheduler(daemon=True)
push_service = fcm.FCMNotification(
    api_key="AAAAVfoH7rk:APA91bFexTVWZ4Kn4tuRXqqccn8dixIX0E2ysNznqrN5z_ZJd6hIJzTvLKAgJCBdc0OrHSO8z5q8PjWGWi-4"
            "-OxgGAqVIYWqxQxWw-THOF70mxLOou3Pn4gDcUnna4NZ1NsNENbvdLjN ")

message_title = "A task's due date is approaching."
content = "hello"
data_payload = {}


def print_ac():
    for user in repairman_collection.find({}):
        print(user['user_ptr_id'])
        device_id = user_collection.find_one({"id": user['user_ptr_id']})['device_id']
        if device_id is None:
            continue

        for task in user['task']:
            dline = task['deadline']


            if dline - datetime.datetime.now() < datetime.timedelta(days=5):
                result = push_service.notify_single_device(registration_id=device_id,
                                                           message_title=message_title,
                                                           message_body=content,
                                                           data_message=data_payload)

                print(result)


sched.add_job(print_ac, 'interval', seconds=10, id="test")
