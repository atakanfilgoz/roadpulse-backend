from itertools import chain

import djongo
from djongo import models
from potholes import *
from rest.utility.ExtendedModels import *


class Address(MetaModel):
    country = models.CharField(default='Turkey', max_length=250)
    district = models.CharField(default='Cankaya', max_length=250)
    city = models.CharField(max_length=250, default='Ankara')
    street = models.CharField(max_length=250, default='')
    street_number = models.CharField(max_length=250, null=True)
    neighbourhood = models.CharField(max_length=250, null=True)
    postal_code = models.CharField(max_length=250, null=True)

    def __str__(self):
        return "{} {} {} {}".format(self.country, self.street, self.city, self.district)

    def formatted_address(self):
        return "{neighbourhood}," \
               " {street}," \
               " {number}," \
               " {postal_code}," \
               " {district}/{city}," \
               " {country}".format(street=self.street,
                                   neighbourhood=self.neighbourhood,
                                   number=self.street_number,
                                   postal_code=self.postal_code,
                                   district=self.district,
                                   city=self.city,
                                   country=self.country)

    class Meta:
        abstract = True


class Image(MetaModel):
    link = models.URLField(
        default="https://image.shutterstock.com/image-vector/pothole-icon-black-filled-vector-260nw-1188459946.jpg")
    public_id = models.CharField(null=True)

    class Meta:
        abstract = True

    def __str__(self):
        return "<public_id: {} , link: {} >".format(self.public_id, self.link)

    def delete(self, **kwargs):
        if self.image.public_id is not None:
            photo_manager.delete_photo(self.image.public_id)
        super().delete(**kwargs)


class PotholeManager(models.DjongoManager):
    @staticmethod
    def delete_all():
        for pothole in Pothole.objects.all():
            pub_id = pothole.image.public_id
            photo_manager.delete_photo(pub_id)
            pothole.delete()

    @staticmethod
    def u_from_dict(dict_obj, p_obj):
        for attr, val in dict_obj.items():
            if not isinstance(val, dict):
                setattr(p_obj, attr, val)
            else:
                PotholeManager.u_from_dict(val, getattr(p_obj, attr))

    @staticmethod
    def reverse_geocode(lat, long):
        from rest import gmaps
        data = {}
        result = gmaps.reverse_geocode((lat, long))
        for component in result[0]['address_components']:
            if "street_number" in component['types']:
                data['street_number'] = component['long_name']
            elif "route" in component['types']:
                data['street'] = component['long_name']
            elif "administrative_area_level_4" in component['types']:
                data['neighbourhood'] = component['long_name']
            elif "administrative_area_level_2" in component['types']:
                data['district'] = component['long_name']
            elif "administrative_area_level_1" in component['types']:
                data['city'] = component['long_name']
            elif "country" in component['types']:
                data['country'] = component['long_name']
            elif "postal_code" in component['types']:
                data['postal_code'] = component['long_name']
        return data


class Pothole(MetaModel):
    HIGH = 'DUSUK'
    MEDIUM = 'ORTA'
    LOW = 'YUKSEK'
    NOT_FIXED = "NOT-FIXED"
    FIXED = "FIXED"

    PRIORITY = [
        (HIGH, 'YUKSEK'),
        (MEDIUM, 'ORTA'),
        (LOW, 'DUSUK')
    ]

    creation_date = models.DateTimeField(auto_now_add=True)
    longitude = models.FloatField(default=0)
    latitude = models.FloatField(default=0)
    label = models.CharField(choices=PRIORITY, default="", max_length=10)
    image = models.EmbeddedField(model_container=Image)
    addr = models.EmbeddedField(model_container=Address)
    status = models.CharField(null=True, max_length=30, default=FIXED)

    objects = PotholeManager()

    def __str__(self):
        return "Created at:{}\n" \
               " Longitude : {}\n" \
               " Latitude : {}\n" \
               " Label : {}\n" \
               " Address: {}".format(self.creation_date, self.longitude, self.latitude, self.label, str(self.addr))


class FixedPothole(MetaModel):
    pothole_id = models.IntegerField()
    images = models.ArrayField(model_container=Image, null=True)

    class Meta:
        abstract = True


class DetectedPotholes(MetaModel):
    user_id = models.IntegerField()
    potholes = models.ListField()
