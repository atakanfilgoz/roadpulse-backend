from django.http import JsonResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from rest.utility.auth_decorators import custom_login_required, role_required
from routes.models import Point
from .models import *
from users.models import *
import json
from datetime import datetime
from rest.utility.JsonToModel import json_to_and_query, mongo_to_dict
from rest.utility.auth_decorators import *
from routes.models import Point
from notifications.notification_task import send_notification2
from notifications.models import ReviewNotification




@csrf_exempt
def assign_task(request):
    json_dict = json.loads(request.body)

    # found which user
    user = Repairman.objects.filter(id=json_dict['assigned_user_id'])[0]

    task = Repair()
    task.deadline = datetime.strptime(json_dict['deadline'], '%m/%d/%y %H:%M:%S')

    route = None

    if "coordinates" in json_dict:
        route = Route()
        point_list = []

        for point in json_dict['coordinates']:
            new_point = Point()
            new_point.latitude = point['latitude']
            new_point.longitude = point['longitude']
            point_list.append(new_point)

        route.points = point_list
        route.save()

    task.assigned_id = json_dict['assigned_user_id']
    task.route = route
    pthls = []
    for p_id in json_dict["potholes"]:
        p_o = Pothole.objects.get(id=p_id)
        wrapper_pothole = WrapperPothole().build_from_super(p_o)
        pthls.append(wrapper_pothole)
    task.potholes = pthls
    task.save()

    if user.task is None:
        new_list = []
    else:
        new_list = user.task

    new_list.append(task)
    user.task = new_list
    user.save()

    review_notf = ReviewNotification()
    review_notf.status = "FORWARDED"
    review_notf.header = "Assignment on Task"
    review_notf.message = "A new task has been assigned to you!"
    review_notf.sender_id = request.user.id
    review_notf.receiver_id = user.id
    review_notf.report_id = user.id #dummy chunk
    send_notification2(review_notf)

    return JsonResponse("Task is successfully assigned.", safe=False)


@csrf_exempt
@custom_login_required
@role_required(roles=["Admin", "Repairman"])
def get_task(request):
    if "Admin" in request.user.roles:
        main_result = []
        assigned = []
        for user in Repairman.objects.all():
            sub_result = {"assigned_id": user.id, "potholes": []}
            for task in user.task:
                potholes = task.to_dict()['potholes']
                for phole in potholes:
                    pthl_id = phole.pop('assign_id')
                    phole['id']=pthl_id
                    phole.pop('pothole_ptr_id')
                    sub_result['potholes'].append(phole)

            main_result.append(sub_result)
        regular_potholes = [p.to_dict() for p in Pothole.objects.all() if p.id not in assigned]
        main_result.append({"not_assigned":regular_potholes})
        return JsonResponse(main_result, safe=False)
    else:
        result = []
        for task in request.user_t.to_dict()['task']:
            potholes = task['potholes']
            for phole in potholes:
                pthl_id = phole.pop('assign_id')
                phole['id'] = pthl_id
                phole.pop('pothole_ptr_id')

            result.append({"assigned_id": task["assigned_id"], "potholes": task['potholes']})
        return JsonResponse(result, safe=False)



@csrf_exempt
@custom_login_required
@role_required(roles=["Admin"])
def delete_task(request, u_id):
    q = []
    q.extend(Repairman.objects.filter(id=u_id))
    if len(q) == 0:
        return JsonResponse("No such user.", safe=False, status=404)

    user = q[0]

    user.task = []
    user.save()
    return JsonResponse("Delete is successful.", safe=False)
