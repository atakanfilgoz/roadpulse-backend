# Generated by Django 2.1 on 2020-04-10 23:08

from django.db import migrations, models
import djongo.models.fields
import potholes.models
import routes.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Detection',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('route', djongo.models.fields.EmbeddedField(model_container=routes.models.Route, null=True)),
                ('create_date', models.DateTimeField(auto_now_add=True)),
                ('assigned_id', models.IntegerField(null=True)),
                ('type', models.CharField(default='Detect', max_length=15)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Repair',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('route', djongo.models.fields.EmbeddedField(model_container=routes.models.Route, null=True)),
                ('create_date', models.DateTimeField(auto_now_add=True)),
                ('deadline', models.DateTimeField()),
                ('assigned_id', models.IntegerField(null=True)),
                ('potholes', djongo.models.fields.ArrayField(model_container=potholes.models.Pothole, null=True)),
                ('type', models.CharField(default='Repair', max_length=15)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
