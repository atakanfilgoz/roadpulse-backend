from django.db import models
from routes.models import Route
from rest.utility.ExtendedModels import *
from djongo import models
from potholes.models import Pothole, Image, Address
from routes.models import *


class Task(MetaModel):
    route = models.EmbeddedField(model_container=Route, null=True)
    create_date = models.DateTimeField(auto_now_add=True)
    deadline = models.DateTimeField()
    assigned_id = models.IntegerField(null=True)
    type = None

    class Meta:
        abstract = True


class Detection(Task):
    deadline = None
    type = models.CharField(max_length=15, default="Detect")
    objects = models.DjongoManager()


class WrapperPothole(MetaModel):
    class Meta:
        abstract = True

    creation_date = models.DateTimeField(auto_now_add=True)
    longitude = models.FloatField(default=0)
    latitude = models.FloatField(default=0)
    label = models.CharField(default="", max_length=10)
    image = models.EmbeddedField(model_container=Image)
    addr = models.EmbeddedField(model_container=Address)
    assign_id = models.IntegerField()
    pothole_ptr_id=models.IntegerField()

    def build_from_super(self, p):
        self.assign_id = p.id
        self.creation_date = p.creation_date
        self.longitude = p.longitude
        self.latitude = p.latitude
        self.label = p.label
        self.image = p.image
        self.addr = p.addr
        return self


class Repair(Task):
    potholes = models.ArrayField(model_container=WrapperPothole, null=True)
    type = models.CharField(max_length=15, default="Repair")
    objects = models.DjongoManager()
